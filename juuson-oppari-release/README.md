# Repository for thesis proof of concept

This Git repository contains the source codes for a proof of concept (PoC) environment as a part of  
Juuso Minkkilä's (student id K1756) thesis *Health checks and service discovery in microservice environment*.

This repository's address is https://gitlab.labranet.jamk.fi/K1756/juuson-oppari and as of now requires 
JAMK LabraNet authentication and an invitation.

Upon release this repository will be partially copied over to a public repository at 
https://gitlab.com/SlightHeadache/jamk-thesis-public-release

The important folders from the point of view of the PoC are `Vagrant` and `Prototypes`. The `Prototypes` 
folder contains the microservice codes used in the PoC along with thier original README files, 
and `Vagrant` contains the Vagrantfile used to deploy the PoC environment.

Please refer to the thesis itself or the repositorio's [setup_notes.md](setup_notes.md) when deploying the PoC.