import akka.http.scaladsl.Http
import modules.AllModules

import scala.util.{Failure, Success}
import cats.syntax.validated._
import com.github.everpeace.healthchecks._
import com.github.everpeace.healthchecks.route._
import com.github.everpeace.healthchecks.HealthCheck.Severity

import scala.concurrent.Future
import scala.util.Random
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import helm._
import ConsulOp.ConsulOpF
import cats.effect.IO
import helm.http4s._
import org.http4s.Uri.uri
import org.http4s.client.blaze.Http1Client
import java.net._

import cats.data.NonEmptyList

object Helpers {
  def easyHealthParam(
     name:                           String,
     id:                             Option[String]   = None,
     interval:                       Option[Interval] = None,
     notes:                          Option[String]   = None,
     deregisterCriticalServiceAfter: Option[Interval] = None,
     serviceId:                      Option[String]   = None,
     initialStatus:                  Option[HealthStatus] = None,
     http:                           Option[String]   = None,
     tlsSkipVerify:                  Option[Boolean]  = None,
     script:                         Option[String]   = None,
     dockerContainerId:              Option[String]   = None,
     tcp:                            Option[String]   = None,
     ttl:                            Option[Interval] = None
   ): HealthCheckParameter = HealthCheckParameter(
    name, id, interval, notes, deregisterCriticalServiceAfter, serviceId, initialStatus, http, tlsSkipVerify, script, dockerContainerId, tcp, ttl
  )
}

// TODO: Change `invalidNel` stuff to `unhealthy()` instead.
object Application extends App {
  val modules = new AllModules

  // https://stackoverflow.com/a/46573787
  val localhost: InetAddress = InetAddress.getLocalHost
  val localIpAddress: String = localhost.getHostAddress

  import modules._

  // TODO: These example checks provided by everpeace guide are redundant
  val example = healthCheck(name = "example", severity = Severity.NonFatal) {
    if (Random.nextBoolean()) healthy else "Unlucky!".invalidNel
  }

  val asyncExample = asyncHealthCheck(name = "asyncExample") {
    Future {
      if (Random.nextBoolean()) healthy else "Unlucky!".invalidNel
    }
  }

  def httpHealthCheck(name: String, target: String, severity: Severity = Severity.Fatal) = asyncHealthCheck(name = name, severity = severity){
    val endpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = target))
    // Mapping Example 5, https://www.programcreek.com/scala/akka.http.scaladsl.model.HttpRequest
    endpointFuture.map {
      response =>
      if (response.status.isSuccess) healthy else unhealthy("${target} not available")
    }
  }

  val rngCheck = asyncHealthCheck(name = "rng health check") {
    val rngEndpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = "http://127.0.0.1:8080/api/rng"))
    // Mapping Example 5, https://www.programcreek.com/scala/akka.http.scaladsl.model.HttpRequest
    rngEndpointFuture.map {
      response =>
      if (response.status.isSuccess) healthy else unhealthy("RNG endpoint not available")
    }
  }

  // Does not really server a purpose?
  val rngCheck2 = httpHealthCheck("rng check 2", "http://127.0.0.1:8080/api/rng")
  val googleCheck = httpHealthCheck(target = "https://google.com", name = "google check", severity = Severity.NonFatal)

  // Consul stuff
  // TODO: Check if runSync can be used instead
  val client = Http1Client[IO]().unsafeRunSync()
  val baseUrl = uri("http://127.0.0.1:8500")

  val interpreter = new Http4sConsulClient(baseUrl, client)
  // TODO: Try-Catch or something, currently crashes if Consul client isn't available on start up
  // TODO: Parameters should come from a configuration file?
  // name, id, interval, notes, deregisterCriticalServiceAfter, serviceId, initialStatus, http, tlsSkipVerify, script, dockerContainerId, tcp, ttl

  import Helpers._
  val endpointHealthCheck: HealthCheckParameter = easyHealthParam(
    name = "RNG endpoint check", http = Some("http://127.0.0.1:8080/api/rng"), interval = Some(Interval.Seconds(15)),
    notes = Some("Created via Helm"), initialStatus = Some(HealthStatus.Critical)
  )

  val primaryHealthCheck: HealthCheckParameter = easyHealthParam(
    name = "General health", http = Some("http://127.0.0.1:8080/health?full=true"), interval = Some(Interval.Seconds(15)),
    notes = Some("Created via Helm"), initialStatus = Some(HealthStatus.Critical)
  )

  // service, id, tags, address, port, enableTagOverride, check, checks
  val registerCommand: IO[Unit] = helm.run(
    interpreter, ConsulOp.agentRegisterService(
      "rng-service", None, None, None, Some(8080), None, None,
      Some(NonEmptyList.of(endpointHealthCheck, primaryHealthCheck))
    )
  )
  // If Consul is not available when this command is run, the whole service fails to start.
  registerCommand.unsafeRunSync()
  
// HealthCheckRoutes.health(example, asyncExample, rngCheck)
  // TODO: Feels like a hack, if health routes are after endpoint routes health routes don't work. Should be using a proper router instead.
  // TODO: IP and port should come from config
  Http().bindAndHandle(HealthCheckRoutes.health(rngCheck, rngCheck2, googleCheck) ~ modules.endpoints.routes, "0.0.0.0", 8080).onComplete {
    case Success(b) => system.log.info(s"application is up and running at ${b.localAddress.getHostName}:${b.localAddress.getPort}")
    case Failure(e) => system.log.error(s"could not start application: {}", e.getMessage)
  }
}
