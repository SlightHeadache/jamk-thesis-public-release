package endpoints

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.{ Failure, Success }

class HealthCheckEndpoint(implicit ec: ExecutionContext, mat: ActorMaterializer, system: ActorSystem) {
  val rngEndpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = "http://127.0.0.1:8080/api/rng"))

  val healthCheckRoute: Route = {
    pathPrefix("api" / "health-check") {
      get { 
        onComplete(rngEndpointFuture) {
          // Nested case
          // https://stackoverflow.com/questions/18708746/scala-pattern-matching-nested-cases
          case Success(res) => res.status.isSuccess match {
            case true => complete("pass")
            case _    => complete("fail: rng unavailable")
          }
          case Failure(_)   => complete("Something went wrong")
        } 
      }
    }
  }
}
