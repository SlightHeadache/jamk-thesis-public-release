package endpoints

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import scala.util.Random

class RNGEndpoint {
  val rng = new Random
  val rngRoute: Route = {
    pathPrefix("api" / "rng") {
      get { complete(rng.nextInt(10).toString) }
    }
  }
}
