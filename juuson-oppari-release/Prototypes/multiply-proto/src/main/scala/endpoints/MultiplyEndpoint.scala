package endpoints

// ActorSystem is needed for Akka HTTP client stuff
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.unmarshalling.Unmarshal

// How to rename imports: https://blog.bruchez.name/2012/06/scala-tip-import-renames.html
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
// A materializer is needed for marshalling
import akka.stream.ActorMaterializer
import helm._
import cats.effect.IO
import helm.http4s._
import org.http4s._
import org.http4s.client.Client
import org.http4s.client.blaze.Http1Client

// Needed for futures
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random
import scala.util.{ Failure, Success }

class MultiplyEndpoint(implicit ec: ExecutionContext, mat: ActorMaterializer, system: ActorSystem) {
  
  val endpointSelector = new Random

  // Request for RNG endpoints from Consul
  // TODO: Same as Application's Helpers.ServiceInstances
  // TODO: Check if runSync can be used instead
  val client: Client[IO] = Http1Client[IO]().unsafeRunSync()
  val baseUrl: org.http4s.Uri = org.http4s.Uri.uri("http://127.0.0.1:8500")
  val interpreter = new Http4sConsulClient(baseUrl, client)
  val fetchServiceNodesCommand: IO[QueryResponse[List[HealthNodesForServiceResponse]]] =
    helm.run(interpreter, ConsulOp.healthListNodesForService("rng-service", None, Some("_agent"), None, None, Some(true), None, None))

  val multiplyRoute: Route = {
    pathPrefix("multiply") {
      (get & path(IntNumber)) { number =>
        val responseData = fetchServiceNodesCommand.unsafeRunSync()
        if (responseData.value.isEmpty) {
          complete(HttpResponse(status = StatusCodes.ServiceUnavailable, entity = "RNG Service unavailable"))
        }
        else {
          // Randomly select used rng instance to mimic load balancing
          val len: Int = responseData.value.length
          val i: Int = endpointSelector.nextInt(len)
          val rngAddress: String = responseData.value(i).node.address
          val rngPort: Int = responseData.value(i).service.port

          val rngUri: Uri = Uri.from(scheme = "http", host = rngAddress, port = rngPort, path = "/api/rng")
          // Actor system required here for HTTP
          val rngEndpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = rngUri))
          onComplete(rngEndpointFuture) {
            case Success(res) =>
              // Materializer required here for Unmarshalling
              val resAsString: Future[String] = Unmarshal(res).to[String]
              onComplete(resAsString) {
                // TODO: Something to catch non-int `n`s, not a big deal though
                case Success(n) => complete((number * n.toInt).toString)
                case Failure(_) => complete(HttpResponse(status = StatusCodes.BadRequest, entity = "Something went wrong"))
              }
            case Failure(_) => complete(HttpResponse(status = StatusCodes.ServiceUnavailable, entity = "RNG Service unavailable"))
          }
        }
      }
    } ~ get {
      complete(HttpResponse(status = StatusCodes.BadRequest, entity = "Please supply a number after the slash"))
    }
  }
}
