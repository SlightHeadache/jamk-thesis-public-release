import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import modules.AllModules

import scala.util.{Failure, Success}
import cats.syntax.validated._
import com.github.everpeace.healthchecks._
import com.github.everpeace.healthchecks.route._
import com.github.everpeace.healthchecks.HealthCheck.Severity

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Random
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshal, Marshaller}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import spray.json._
import cats.effect.IO
import helm._
import helm.http4s._
import org.http4s.client.Client
import org.http4s.client.blaze.Http1Client

// https://doc.akka.io/docs/akka-http/current/common/json-support.html
case class MinimalService(Service: String, Port: Int)
case class TaggedAddresses(lan: String, wan: String)
case class ExternalService(Datacenter: String, Node: String, Address: String, Service: MinimalService, NodeMeta: Map[String, String] = Map("external-owner"->"google"), TaggedAddresses: TaggedAddresses)
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val minimalServiceFormat: RootJsonFormat[MinimalService] = jsonFormat2(MinimalService)
  implicit val taggedAddressFormat: RootJsonFormat[TaggedAddresses] = jsonFormat2(TaggedAddresses)
  implicit val externalServiceFormat: RootJsonFormat[ExternalService] = jsonFormat6(ExternalService)
}

object Helpers {
  def serviceInstances(service: String, passing: Option[Boolean] = None)(implicit executionContext: ExecutionContext):
  QueryResponse[List[HealthNodesForServiceResponse]] = {
    val client: Client[IO] = Http1Client[IO]().unsafeRunSync()
    val baseUrl: org.http4s.Uri = org.http4s.Uri.uri("http://127.0.0.1:8500")
    val interpreter = new Http4sConsulClient(baseUrl, client)
    helm
      .run(interpreter, ConsulOp.healthListNodesForService(service, None, None, None, None, passing, None, None))
      .unsafeRunSync()
  }
}

//val TTLTick = "update"
case class ServiceStatus(Status: String, Output: String)
trait CheckJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val serviceStatusFormat: RootJsonFormat[ServiceStatus] = jsonFormat2(ServiceStatus)
}

class TTLActor(checkId: String)(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer) extends Actor with CheckJsonSupport {
  import Helpers._

  def updateStatus(checkId: String, serviceStatus: ServiceStatus): Future[HttpResponse] = {
    Marshal(serviceStatus).to[RequestEntity].flatMap { entity =>
      Http().singleRequest(
        HttpRequest(uri = Uri("http://127.0.0.1:8500/v1/agent/check/update/" + checkId), method = HttpMethods.PUT, entity = entity)
      )
    }
  }

  val endpointSelector = new Random
  def receive: PartialFunction[Any, Unit] = {
    case "update" =>
      // fetch search service from consul

      // This seems to cause errors?
      // Fixed by setting meta and tagged addresses for external service
      val instances: QueryResponse[List[HealthNodesForServiceResponse]] = serviceInstances("search")
      if (instances.value.isEmpty) updateStatus(checkId = checkId, serviceStatus = ServiceStatus("failure", "No search instances found")).onComplete{
        case Success(_) => system.log.info(s"TTL status updated to 'failure' with message 'No search instances found'")
        case Failure(exception) => system.log.error(s"Error while updating TTL status: {}", exception.getMessage)
      }
      else {
        // endpoint selection could be made into a library?
        val len: Int = instances.value.length
        val i: Int = endpointSelector.nextInt(len)
        // here RNG refers to a randomly selected endpoint, NOT the RNG service
        val rngAddress: String = instances.value(i).node.address
        val rngPort: Int = instances.value(i).service.port

        // Used to get search, but output was excessive
        val rngUri: Uri = Uri.from(scheme = "https", host = rngAddress, port = rngPort)
        system.log.info(s"Checking address: {}", rngUri)

        // check search return code, 2XX passing, no response fails, else warning
        Http().singleRequest(HttpRequest(uri = rngUri, method = HttpMethods.HEAD)).onComplete {
          case Success(res) =>
            Unmarshal(res).to[String].onComplete {
              case Success(_) =>
                if (res.status.isSuccess()) {
                  updateStatus(checkId = checkId, serviceStatus = ServiceStatus("passing", res.headers.toString())).onComplete {
                    case Success(_) => system.log.info(s"TTL status updated to 'passing' with message '{}'", res.headers.toString())
                    case Failure(exception) => system.log.error(s"Error while updating TTL status: {}", exception.getMessage)
                  }
                }
                else {
                  updateStatus(checkId = checkId, serviceStatus = ServiceStatus("warning", res.headers.toString())).onComplete {
                    case Success(_) => system.log.info(s"TTL status updated to 'warning' with message '{}'", res.headers.toString())
                    case Failure(exception) => system.log.error(s"Error while updating TTL status: {}", exception.getMessage)
                  }
                }
              case Failure(exception) => system.log.error(s"Error in TTL health check: {}", exception.getMessage)
            }
          case Failure(exception) =>
            system.log.error(s"Error in TTL health check: {}", exception.getMessage)
            val status: String = "Error while fetching health: " + exception.getMessage
            updateStatus(checkId = checkId, serviceStatus = ServiceStatus("failure", status)).onComplete {
              case Success(_) => system.log.info(s"TTL status updated to 'failure' with message '{}'", status)
              case Failure(e) => system.log.error(s"Error while updating TTL status: {}", e.getMessage)
            }
        }
      }
  }
}

object Application extends App with JsonSupport {
  val modules = new AllModules

  import modules._

  // TODO: Change `invalidNel` stuff to `unhealthy()` instead.
  def httpHealthCheck(name: String, target: String, severity: Severity = Severity.Fatal) = asyncHealthCheck(name = name, severity = severity){
    val endpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = target))
    // Mapping Example 5, https://www.programcreek.com/scala/akka.http.scaladsl.model.HttpRequest
    endpointFuture.map {
      response =>
      if (response.status.isSuccess) healthy else unhealthy("Multiply endpoint not available")
    }
  }

  // Adapted from http://pawelgebal.com/articles/post-json-with-akka-http/
  def registerExternalService(service: ExternalService): Future[HttpResponse] = {
    Marshal(service).to[RequestEntity].flatMap { entity =>
      Http().singleRequest(
        HttpRequest(uri = Uri("http://127.0.0.1:8500/v1/catalog/register"), method = HttpMethods.PUT, entity = entity)
      )
    }
  }

  val example = healthCheck(name = "example", severity = Severity.NonFatal) {
    if (Random.nextBoolean()) healthy else "Unlucky!".invalidNel
  }

  val asyncExample = asyncHealthCheck(name = "asyncExample") {
    Future {
      if (Random.nextBoolean()) healthy else "Unlucky!".invalidNel
    }
  }

  // TODO: Could use Helpers.serviceInstances?
  val rngAvailabilityCheck = asyncHealthCheck(name = "check rng endpoint availability", severity = Severity.Fatal) {
    // TODO: Shared interpreter between modules?
    // Consul stuff
    val client: Client[IO] = Http1Client[IO]().unsafeRunSync()
    val baseUrl: org.http4s.Uri = org.http4s.Uri.uri("http://127.0.0.1:8500")
    val interpreter = new Http4sConsulClient(baseUrl, client)

    val fetchServiceNodesCommand: IO[QueryResponse[List[HealthNodesForServiceResponse]]] =
      helm.run(interpreter, ConsulOp.healthListNodesForService("rng-service", None, None, None, None, Some(true), None, None))

    // Some sort of future thing, maybe?
    val responseFuture = fetchServiceNodesCommand.unsafeToFuture()
    responseFuture.map {
      response =>
      if (response.value.nonEmpty) healthy else unhealthy("No RNG endpoints available")
    }
  }

  val multiplyCheck = asyncHealthCheck(name = "multiply health check") {
    val rngEndpointFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = "http://127.0.0.1:8082/multiply/1"))
    // Mapping Example 5, https://www.programcreek.com/scala/akka.http.scaladsl.model.HttpRequest
    rngEndpointFuture.map {
      response =>
      if (response.status.isSuccess) healthy else unhealthy("Multiply endpoint not available")
    }
  }

  val multiplyCheck2 = httpHealthCheck("multiply check 2", "http://127.0.0.1:8082/multiply/1")
  val googleCheck = httpHealthCheck(target = "https://google.com", name = "google check", severity = Severity.NonFatal)

  registerExternalService(
    ExternalService(
      "dc1", "google", "www.google.com", MinimalService("search", 443), Map("external-owner"->"google"),
      TaggedAddresses("www.google.com", "www.google.com"))
  ) onComplete {
    case Success(response) =>
      Unmarshal(response).to[String].onComplete {
        case Success(value) =>
          if (value == "true") system.log.info(s"External service registered successfully")
          else system.log.error(s"An error occurred while registering external service")
        case Failure(exception) => system.log.error(s"External service registration error: {}", exception.getMessage)
      }
    case Failure(exception) => system.log.error(s"External service not registered: {}", exception.getMessage)
  }

  // https://doc.akka.io/docs/akka/2.5/scheduler.html
  import system.dispatcher
  val ttlActor = system.actorOf(Props(new TTLActor("google-check")))
  val cancellable =
    system.scheduler.schedule(
      30.seconds, 60.seconds, ttlActor,"update"
    )

  // TODO: Feels like a hack, if health routes are after endpoint routes health routes don't work. Should be using a proper router instead?
  Http().bindAndHandle(HealthCheckRoutes.health(multiplyCheck, multiplyCheck2, googleCheck, rngAvailabilityCheck) ~ modules.endpoints.routes, "0.0.0.0", 8082).onComplete {
    case Success(b) => system.log.info(s"application is up and running at ${b.localAddress.getHostName}:${b.localAddress.getPort}")
    case Failure(e) => system.log.error(s"could not start application: {}", e.getMessage)
  }
}
