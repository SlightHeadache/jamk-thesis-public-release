package modules

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.softwaremill.macwire._
import endpoints.{Endpoints, HealthCheckEndpoint, MultiplyEndpoint}

import scala.concurrent.ExecutionContext

class AllModules extends EndpointModule

trait EndpointModule extends AkkaModules {
  lazy val healthCheckEndpoint = wire[HealthCheckEndpoint]
  lazy val rngEndpoint = wire[MultiplyEndpoint]

  lazy val endpoints = wire[Endpoints]
}

trait AkkaModules {
  implicit lazy val system = ActorSystem("simpleHttpServerJson")
  implicit lazy val materializer = ActorMaterializer()
  implicit lazy val executor: ExecutionContext = system.dispatcher
}