This folder contains the prototype services used for demonstrating service discovery and health checks.
Despite lacking elegance, the services should demonstrate their goals sufficiently. 

Both services are based on mongodb example here: https://github.com/gabfssilva/akka-http-microservice-templates and have their original README files.
