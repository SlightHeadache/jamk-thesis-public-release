# Deploying the demo environment

## Pre-requisites

* Vagrant (tested with version 2.2.4)
* VirtualBox (tested with version 6.0.4)
* Clone of this repository

## Starting the machines

To start up the virtual machines required to run the demo environment, locate the `Vagrant` folder of this project. 
Once in that folder, run comman `vagrant up` in Powershell to provision and start the machines.

*NOTE!* If a machine fails to get provisioned due to SSH errors, shut down and remove the failed machine 
with its files from the VirtualBox UI, and run `vagrant up` again.

## Starting the Consul cluster

Once all the virtual machines are up and running open six separate Powershell windows. In each of those windows, 
connect to one of the machines with command `vagrant ssh $machine` where `$machine` is one of the 
following: `s1`, `s2`, `s3`, `c1`, `c2`, `c3`.

After you are connected to each of the machines, run `consul agent -config-dir=/etc/consul.d` on each one of them, 
and the Consul cluster should get started.

*NOTE!* Closing the window where Consul agent is running means that the Consul agent stops! Thus it is currently 
advicable to leave Consul agent windows open.

## Starting the services

When everything is ready for launching the services open three additional Powershell windows. 
Connect to `c1`, `c2` and `c3` in them. In `c1`, navigate to folder `multiply-proto` and run `sbt` command 
to open sbt shell. Once the shell is open, run `;clean;compile;reStart` or just `reStart` to compile and start the service. 
Do the same thing in `c2` and `c3` as well, but substitute folder `multiply-proto` with `rng-proto`. 
Don't worry if there are some dependency warnings, those can be safely ignored. Now the service should be up and running.

## Common issues

*Vagrant SSH fails to connect to a running machine.* If Vagrant fails to connect to a machine, restart that machine from 
VirtualBox UI. The issue seems to be caused by VirtualBoxManager, and doesn't have a proper fix as far as I know. 
However, VirtualBox Guest Additions being installed to the machines during provisioning should alleviate the issue a bit, 
but unfortunately that doesn't apply during the first launch.
